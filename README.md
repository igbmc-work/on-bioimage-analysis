# On bioimage analysis: what is is and why it's important
## Presentation of bioimage analysis at IGBMC
Brief presentation to introduce basic concepts and usefulness of bioimage analysis in general and in the context of IGBMC.

## Author
Marco Dalla Vecchia (dallavem@igbmc.fr)

## Availability
Slides are available [here](https://igbmc.gitlab.io/mic-photon/on-bioimage-analysis/)
