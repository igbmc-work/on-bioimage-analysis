# Import dependencies
from utils import *

# Define search query
query = "(igbmc[Affiliation]) AND (imaging[All Fields] OR microscopy[All Fields])"

# Define path where to store found papers
path = "assets/data/"

# Define max number of results
max_results = 400

# Actually find and save papers
save_papers_text_to_disk(query, path, max_results)
