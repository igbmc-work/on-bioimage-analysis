from Bio import Entrez
import re

def get_ids_from_query(query, result_number):
    """
    access Pubmed and retrieve result_number of IDs of articles found using query
    """
    Entrez.email = 'dallavem@igbmc.fr'
    handle = Entrez.esearch(db='pubmed', 
                            sort='relevance', 
                            retmax=str(result_number),
                            retmode='xml', 
                            term=query)
    results = Entrez.read(handle)
    return results['IdList']


def fetch_details(id_list):
    """
    Use found IDs to retrieve full text from Pubmed
    """
    ids = ','.join(id_list)
    Entrez.email = 'dallavem@igbmc.fr'
    handle = Entrez.efetch(db='pubmed',
                 retmode='text',
                 id=ids,
                 rettype='abstract')
    results = handle.read()
    return results

def save_papers_text_to_disk(query, output_path, max_num_papers):

    ids = get_ids_from_query(query, 400)
    content = str(fetch_details(ids))# use the sampled IDs to fetch paper full texts

    with open(output_path+'papers.txt', 'w') as f:
        f.write(content)

def extract_abstracts(input_path):
    """
    Find text piece between author information and the next break line. This should be the Abstract text in most cases.
    """
    with open(input_path, 'r') as f:
        text = f.read()

    author_info_list = []
    for m in re.finditer('Author information: \n', text):
        author_info_list.append(m.end())

    abstract_start_ids = []
    for line_id in author_info_list:
        abstract_start_ids.append(2+line_id+text[line_id:].find("\n\n"))

    abstracts = []
    for line_id in abstract_start_ids:
        abstract_end = text[line_id:].find('\n\n')
        abstracts.append(text[line_id: line_id+abstract_end].replace('\n', ' '))
    
    return abstracts
